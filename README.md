## Less

нужно использовать less-компилятор (WinLess\Koala\SimpleLess\сборщики)

точка входа - /libs/css/common.less

выгружаемый файл - /libs/css/common.css

## Версионность

для файлов common.less, script.js, map.js жестко зашита версионность (?v=08042016)

ToDo нужно ее генерить динамически на основе последнего времени изменения файла