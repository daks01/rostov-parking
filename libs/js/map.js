var map, geo_objects, myCarousel;

/**
 * @param {object[]} coordinates
 */
function initMap(coordinates){
    var map_container = 'map',
        map_center = [47.252213, 39.693597],
        map_zoom = 13;

    ymaps.ready(function(){
        map = new ymaps.Map(map_container, {
            center: map_center,
            zoom: map_zoom
        });

        map.behaviors.disable('scrollZoom');

        if(!coordinates || !coordinates.length){
            return;
        }

        geo_objects = map.geoObjects;

        $.each(coordinates, function(index, coordinate){
            var default_options = {
                    iconLayout: 'default#image',
                    iconImageHref: '/libs/img/map-pin.png',
                    iconImageSize: [46, 46],
                    hideIconOnBalloonOpen: false,
                    balloonOffset: [5, -46]
                },
                placemark = new ymaps.Placemark([coordinate.lat, coordinate.lng], {
                    balloonContent: coordinate.balloon,
                    hintContent: coordinate.hint
                }, jQuery.extend(default_options, coordinate.options));

            placemark['id'] = coordinate.id;

            if(coordinate.events && coordinate.events.balloonopen){
                placemark.events.add('balloonopen', function(event){
                    coordinate.events.balloonopen(event, placemark);
                });
            }

            geo_objects.add(placemark);
        });
    });
}

/**
 * Callback for 'balloonopen' event for ajax load content of balloon
 *
 * @param {object} event
 * @param {object} placemark
 *
 * @private
 */
function _loadBalloonContent(event, placemark){
    $.get('balloon.html', {'id': placemark.id}, function(response){
        // Get address by coordinates of placemark
        ymaps.geocode(placemark.geometry.getCoordinates(), {
            results: 1
        }).then(function(result){
            var address = result.geoObjects.get(0) ?
                result.geoObjects.get(0).properties.get('name') :
                'Не удалось определить адрес.';

            response = response.split('{address}').join(address);
            response = response.split('{id}').join(placemark.id);
        }).done(function(){
            placemark.properties.set('balloonContent', response);
            if ($.isFunction(Carousel)) {
                myCarousel = new Carousel({delay: 25});
            }
        });
    });
}

jQuery(document).ready(function(){

    initMap([
        {
            id: 1, // Placemark ID
            lat: 47.253700,
            lng: 39.692213,
            balloon: 'Идет загрузка данных...', // Preload text
            events: {balloonopen: _loadBalloonContent}
        },
        {
            id: 2, // Placemark ID
            lat: 47.251705,
            lng: 39.652215,
            balloon: 'Идет загрузка данных...', // Preload text
            events: {balloonopen: _loadBalloonContent}
        }
    ]);

});
