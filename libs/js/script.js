/*
 * mapResize
 */
MapResize = (function (module) {
    'use strict';
    var mapBlock,
        resizeMap,
        bindEvents,
        module = function () {};

    module.mapHeight  = function () {
        return $(window).height() - $('.map-block__menu').outerHeight(true) - $('.map-block__statistics').outerHeight(true);
    }

    resizeMap  = function () {
        mapBlock.height(module.mapHeight());
    }

    bindEvents = function () {
        resizeMap();

        $(window).resize(function(){
            resizeMap();
        });
    }

    module.init = function () {
        mapBlock = $('.map-block__parking-map');

        bindEvents();
    }

    return module;

})();


/*
 * Toggle fullscreen map 
 */
ToggleFullscreenMap = (function (module) {
    'use strict';
    var btn,
        mapBlock,
        baseMapHeight,
        toggleFullsize,
        isFullsize,
        createLink,
        bindEvents,
        module = function () {};

    toggleFullsize = function (height){
        mapBlock.height( height );
        //Ymap resize
        map.container.fitToViewport();
    }
    
    isFullsize = function (){
        if(btn.hasClass('active')) {
            //MapResize module
            toggleFullsize( MapResize.mapHeight() );
        } else {
            toggleFullsize( baseMapHeight );
        }
    }

    bindEvents = function () {
        btn.click(function(){
            var self = $(this);
            self.toggleClass('active');
            isFullsize();
        });
        
        $(window).resize(function(){
            isFullsize();
        });
    }

    module.init = function () {
        btn = $('[data-fullsizemap-btn]');
        mapBlock = $('.map-block__parking-map');
        baseMapHeight = mapBlock.outerHeight(true);

        bindEvents();
    }

    return module;

})();


/*
 * Create <a href="tel:#######"> on mobile
 */
CreatePhoneLink = (function (module) {
    'use strict';
    var el,
        createLink,
        bindEvents,
        module = function () {};

    createLink = function (elem) {
        elem.wrap("<a href='tel:" + elem.data('tel') + "' class='link-icon'></a>")
    }

    bindEvents = function () {
        if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
            el.each(function(){
                createLink( $(this) );
            });
        }
    }

    module.init = function () {
        el = $('[data-tel]');

        bindEvents();
    }

    return module;

})();

/*
 * Toggle hidden content
 */
ToggleContent = (function (module) {
    'use strict';
    var btn,
        btnClose,
        toggleTarget,
        bindEvents,
        module = function () {};

    toggleTarget = function (opt) {
        var name = opt.elem.data('toggle'),
            target = $('#' + name).length ? $('#' + name) : $('[data-toggle-target=' + name + ']');

        if(opt.close) {
            target.hide();
            opt.elem.removeClass('active');
        } else {
            if(target.is(':visible')) {
                opt.elem.addClass('active');
            }
            target.toggle();
            opt.elem.toggleClass('active');
        }
    }

    bindEvents = function () {
        btn.click(function(){
            toggleTarget({
                elem: $(this),
                close: false
            });
        });
        btnClose.click(function(){
            toggleTarget({
                elem: $('[data-toggle=' + $(this).data('toggle-close') + ']'),
                close: true
            });
        });
    }

    module.init = function () {
        btn = $('[data-toggle]');
        btnClose = $('[data-toggle-close]');

        bindEvents();
    }

    return module;

})();

/*
 * Scroll to anchor
 */
ScrollToAnchor = (function (module) {
    'use strict';
    var anchorLinkSelector,
        scrollTo,
        prevClickedSidebarLink,
        prevClickedLink,
        addPin,
        addBorder,
        link,
        toggleSteps,
        toggleTabs,
        bindEvents,
        module = function () {};

    //ToDo обобщить
    addPin = function (elem) {
        if(prevClickedLink !== undefined){
            prevClickedLink.removeClass('active');
        }
        elem.addClass('active');
        prevClickedLink = elem;
    }

    addBorder = function (elem) {
        if(prevClickedSidebarLink !== undefined){
            prevClickedSidebarLink.removeClass('active');
        }
        elem.addClass('active');
        prevClickedSidebarLink = elem;
    }

    toggleSteps = function (elem, link) {
        if (elem.attr('data-step') !== undefined) {
            var linkElem = link || $('[href=#' + elem.attr('id') + ']', '.menu-2');

            $('[data-step]').hide();
            elem.show();
            addPin(linkElem);
        }
    }

    toggleTabs = function (elem, link) {
        if (elem.attr('data-tab') !== undefined) {
            var linkElem = link || $('[href=#' + elem.attr('id') + ']');

            $('[data-tab]').hide().removeClass('active');
            elem.show().addClass('active');
            addBorder(linkElem);
        }
    }

    scrollTo = function (target, link) {
        var targetElem = $(target),
            offsetTop;
        if (target && target !== '#' && targetElem.offset()) {
            //ToDo menu-2 вынести уже в переменную
            offsetTop = targetElem.is("[data-tab]") || targetElem.is("[data-step]")
                ? $('.menu-2').offset().top
                : targetElem.offset().top;

            toggleSteps(targetElem, link);

            toggleTabs(targetElem, link)


            $("html:not(:animated),body:not(:animated)").stop().animate({ scrollTop: offsetTop }, {
                queue: false, duration: 500, complete: function () {
                    if (history && history.replaceState) {
                        history.replaceState({}, "", target);
                    } else {
                        window.location.hash = target;
                    }
                }
            }, 500);
        }
    }

    bindEvents = function () {
        var urlHash = window.location.hash;
        toggleSteps($('[data-step]').first());
        toggleTabs($('[data-tab]').first());

        $(document).on('click', anchorLinkSelector, function (event) {
            event.preventDefault();
            var targetId = $(this).attr('href');

            scrollTo(targetId, $(this));
        });

        if(urlHash && $(urlHash).length) {
            var targetId = urlHash;

            scrollTo(targetId, $('[href^='+urlHash+']', '.menu-2'));
            toggleSteps($(targetId));
            toggleTabs($(targetId));
        }
    }

    module.init = function () {
        anchorLinkSelector = 'a[href^="#"]';

        bindEvents();
    }

    return module;

})();

ToggleFullscreenMap.init();

CreatePhoneLink.init();

ToggleContent.init();

ScrollToAnchor.init();
